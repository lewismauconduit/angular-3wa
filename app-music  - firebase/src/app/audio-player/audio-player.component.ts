import { Component, OnInit, OnDestroy, OnChanges, ɵConsole } from '@angular/core';
import { AlbumService } from '../album.service';
import { interval, Observable } from 'rxjs';
import { takeWhile, count } from 'rxjs/operators';

@Component({
  selector: 'app-audio-player',
  templateUrl: './audio-player.component.html',
  styleUrls: ['./audio-player.component.scss']
})
export class AudioPlayerComponent implements OnInit {

  showplayer: boolean;
  albumPlayed;
  numberOfTitle: number;
  titlePlayed: number;
  ratio: number;
  count: number;
  timer;

  constructor(private albumService: AlbumService) { }

  ngOnInit() {

    this.albumService.subjectAlbum.subscribe(data => {

      this.albumPlayed = data;
      this.albumService.getAlbumList(data.id).subscribe(
                                albumList => {
                                      this.numberOfTitle = albumList.list.length;
                                      this.ratio = (1 / this.numberOfTitle) * 100;
                                }
      );





      this.titlePlayed = 1;
      this.count = 1;
      this.timer = null;

        this.showplayer = true;

        this.timer = setInterval(() => {

              this.count++;
              if (this.count % 12 == 0) {
                this.titlePlayed++;
                this.ratio = (this.titlePlayed / this.numberOfTitle) * 100
              }
              if (this.count == 30) 
              {
                  clearInterval(this.timer);
                  this.showplayer = false;
                  this.albumService.switchOff(data);
              }
              console.log(this.count);
            }, 1000);

        /*
        const count = interval(1000);
        const pipeCount = count.pipe(takeWhile(value => value < data.duration));

        pipeCount.subscribe(value => {
                console.log(value);
                this.ratio = (value / data.duration)*100;
               if (value % 120 == 0)
          {
            this.titlePlayed++;
          } 
            });
          */
      

    })

  }

}
