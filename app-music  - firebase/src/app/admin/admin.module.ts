import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { GuardService } from '../guard.service';
import { AlbumComponent } from './album/album.component';
import { ShareModule } from '../share/share.module';
import { AddAlbumComponent } from './add-album/add-album.component';


const albumsRoutes: Routes = [
  { 
    path: 'admin/add-album', canActivate: [GuardService], 
    component: AddAlbumComponent 
  }
];


@NgModule({
  declarations: [
    AlbumComponent, 
    AddAlbumComponent
  ],

  imports: [
    CommonModule,
    ShareModule,
    RouterModule.forRoot(albumsRoutes)
    
  ],
  exports : [
    AlbumComponent,
    RouterModule]
})
export class AdminModule { }
