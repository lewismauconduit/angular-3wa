import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { AlbumService } from '../../album.service';
import { Album } from '../../album';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-album',
  templateUrl: './add-album.component.html',
  styleUrls: ['./add-album.component.scss']
})
export class AddAlbumComponent implements OnInit {

  addAlbumForm: FormGroup;

  constructor(  private formBuilder: FormBuilder,
                private albumService: AlbumService,
                private router: Router
              ) 
  { 

  }

  ngOnInit() 
  {
    this.addAlbumForm = this.formBuilder.group({
                                  name: new FormControl('', [
                                            Validators.required,
                                            Validators.minLength(5)
                                          ]),
                                })
  }

  get name()
  {
    return this.addAlbumForm.get('name');
  }

  onSubmit() 
  {
    let album: Album = {
      id: "",
      name: this.addAlbumForm.value['name'],
      title: "",
      ref: "",
      duration: 900,
      description: "",
      status: "off"
    }
/*
    let id;
    this.albumService.getLastAlbumId().subscribe(value => id = value);
*/
    //album.id = "";
    //album.name = this.addAlbumForm.value['name'];
    

    this.albumService.addAlbum(album).subscribe(
          album => { console.log(album) },
          error => console.error(error),
          () => {
              this.router.navigate(['/admin'], { queryParams: { message: 'success' } });
          }
      );
  }

}
