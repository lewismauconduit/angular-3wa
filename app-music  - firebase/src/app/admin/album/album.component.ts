import { Component, OnInit } from '@angular/core';
import { AlbumService} from '../../album.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {

  albums;
  listLength: number = 5;

  constructor(private albumService: AlbumService,
              private router: Router) 
  {

  }

  ngOnInit() 
  {
    //Récupère une liste d'albums limités par la variable d'env listLength
    this.albumService.paginate(0, this.listLength).subscribe(
        albums => {
              this.albums = albums;
        }
      )

  }

  onClickUpdate(album)
  {
      console.log(album)
  }

  onClickDelete(album)
  {

    this.albumService.deleteAlbum(album.id).subscribe(
              () => console.log('dfsf')
    );
  }

  showPaginate(groupOfAlbums)
  {
    this.albums = groupOfAlbums;
  }

  onClickAdd()
  {
    this.router.navigate(['/admin/add-album']);
  }
}
