import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import * as firebase from 'firebase';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlbumsComponent } from './albums/albums.component';
import { AlbumDetailsComponent } from './album-details/album-details.component';
import { AlbumDescriptionComponent } from './album-description/album-description.component';
import { SearchComponent } from './search/search.component';
import { LoginComponent } from './login/login.component';
import { AudioPlayerComponent } from './audio-player/audio-player.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GuardService } from './guard.service';
import { AdminModule } from './admin/admin.module';
import { ShareModule } from './share/share.module';


// définition de la constante pour les routes
const albumsRoutes: Routes = [
  {
    path: 'albums',
    component: AlbumsComponent
  },
  {
    path: '',
    redirectTo: '/albums',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'album/:id',
    component: AlbumDescriptionComponent
  },
  {
    path: 'dashboard', canActivate: [GuardService],
    component: DashboardComponent
  }
  
];

const firebaseConfig = {
  apiKey: "AIzaSyCbV6qWZTIEJHugQNWdFshpNGGyBimbei0",
  authDomain: "NAME_BD",
  databaseURL: "https://app-music-f9ca9.firebaseio.com",
  projectId: "app-music-f9ca9",
  storageBucket: "STORAGE",
  messagingSenderId: "MESSAGING_ID"
};

firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [
    AppComponent,
    AlbumsComponent,
    AlbumDetailsComponent,
    SearchComponent,
    LoginComponent, // chargement des routes dans l'application
    AlbumDescriptionComponent,
    AudioPlayerComponent, 
    DashboardComponent
    
  ],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(albumsRoutes),
    HttpClientModule,
    AdminModule,
    ShareModule
  ],
  exports: [
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
