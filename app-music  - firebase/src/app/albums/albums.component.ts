import { Component, OnInit  } from '@angular/core';
import { environment } from '../../environments/environment';

import { Album } from '../album';
import { AlbumService } from '../album.service';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss'],
  
})
export class AlbumsComponent implements OnInit {

  titlePage: string = "Page principale Albums Music";
  albums;
  selectedAlbum: Album; //transmis à AlbumDetailsComponent
  playedAlbum: Album; //album renvoyé par AlbumDetails quand on clique sur play
  albumSearched: Album[]; //album renvoyé à la recherche
  

   // renvoyée aussi dans paginateComponent

  constructor( private albumService: AlbumService ) 
  { 
    
  }

  ngOnInit() 
  {

    //Récupère une liste d'albums limités par la variable d'env listLength
    this.albumService.paginate(0, environment.listLength).subscribe(
      albums => this.albums = albums
      )
  }

  onSelect(album: Album)
  {
    this.selectedAlbum = album;
  }

  playParent(album: Album)
  {
    //this.albumService.getAlbums().forEach(element => this.albumService.switchOff(element));
    this.playedAlbum = album;
    this.albumService.switchOn(album);
  }

  getSearch(album: Album[])
  {
    this.albumSearched = album;
  }

  showPaginate(groupOfAlbums: Album[])
  {
    this.albums = groupOfAlbums;
  }

}
