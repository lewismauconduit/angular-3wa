import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    // ...
  } from '@angular/animations';

import { Album } from '../album';
import { AlbumService } from '../album.service';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss'],
  animations: [
    trigger('playing', [
      // ...
      state('play', style({
        backgroundColor: '#A9E8FF'
      })),
      state('notplay', style({
        backgroundColor: 'white'
      })),
      transition('play => notplay', [
        animate('1s')
      ]),
      transition('notplay => play', [
        animate('2s')
      ]),
    ]),
  ]
})
export class AlbumDetailsComponent implements OnInit {

  @Input() album: Album;
  @Output() onPlay: EventEmitter<Album> = new EventEmitter();

  albumChoosen;
  isPlay: boolean;

  constructor(private albumService: AlbumService) 
  {

  }

  ngOnChanges()
  {
   
    if(this.album)
    {
    // récupération de la liste des chansons
      this.albumService.getAlbumList(this.album.id).subscribe(
                                  album => this.albumChoosen = album
      );
      this.isPlay = false;
      
    }
    
  }

  ngOnInit() 
  {
    
  }
  
  play(album: Album)
  {
    this.onPlay.emit(album);  // émettre un album vers le parent
    this.isPlay = true;
  }
}
