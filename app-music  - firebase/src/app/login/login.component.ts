import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../auth-service.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthServiceService,
              private router: Router) 
  { 

  }

  ngOnInit() {
  }

  onSubmit(form: NgForm)
  {
    let email = form.value['email'];
    let password = form.value['password'];
    
    this.authService.auth(email, password).then(value => this.router.navigate(['/admin']))
                                          .catch(error => console.log(error.message));

                                          
  }

}
