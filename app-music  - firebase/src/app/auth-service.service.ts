import { Injectable } from '@angular/core';
// modules nécessaires pour l'authentification
import * as firebase from 'firebase/app';
import 'firebase/auth';
import { Router } from '@angular/router';

import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  private authState: boolean = null;

  // subject pour l'authentification 
  subjectConnected = new Subject<boolean>();

  constructor(private router: Router) 
  { 
    // Observable: teste si l'utilisateur est connecté
    firebase.auth().onAuthStateChanged( (user) => {
                            if (user) {
                              this.authState = true;
                            } 
                            else 
                            {
                              this.authState = false;
                            }
                            this.subjectConnected.next(this.authState);
                        });

    
  }

  auth(email: string, password: string): Promise<any> 
  {
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

  logOut()
  {
    this.subjectConnected.next(this.authState);
    return firebase.auth().signOut();
  }
  authenticated()  // utilisée dans le Guard Service
  {
    return this.authState;
  }
}
