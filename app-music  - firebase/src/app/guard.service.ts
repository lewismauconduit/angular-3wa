import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { AuthServiceService } from './auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {

  constructor(private authService: AuthServiceService,
              private router: Router) 
  { 

  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): any | boolean 
    
  {
      if (this.authService.authenticated()) return true;
      else 
      {
        this.router.navigate(['/login'])
        return false;
      }
  }
    
}
