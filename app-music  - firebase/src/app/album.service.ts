import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// Opérateurs de RxJS
import { map, last } from 'rxjs/operators';
// libraire utile pour le traitement de données
import * as _ from 'lodash';

import { Album } from './album';
import { AlbumList } from './album-list';
import { ALBUMS } from './mock-albums';
import { ALBUM_LISTS } from './mock-albums';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})

export class AlbumService {

  private _albums: Album[] = ALBUMS;
  private _albumList: AlbumList[] = ALBUM_LISTS;

  // subject pour la pagination - informer les autres components
  sendCurrentNumberPage = new Subject<number>();

  // subject pour la barre de lecture 
  subjectAlbum = new Subject<Album>();

  // convention dans l'API ajoutez votre identifant de base de données
  private albumsUrl = 'https://app-music-f9ca9.firebaseio.com/albums';
  private albumListsUrl = 'https://app-music-f9ca9.firebaseio.com/albumLists';

  constructor(private http: HttpClient) {

  }

  getAlbums(): Observable<Album[]> {
    return this.http.get<Album[]>(this.albumsUrl + '/.json', httpOptions).pipe(
      // Préparation des données avec _.values pour avoir un format exploitable dans l'application => Array de values JSON
      map(albums => _.values(albums)),
      // Ordonnez les albums par ordre de durées décroissantes
      map(albums => {
        return albums.sort(
          (a, b) => { return b.duration - a.duration }
        );
      })
    )
  }

  getAlbum(id: string): Observable<Album> {
    // URL/ID/.json pour récupérer un album
    return this.http.get<Album>(this.albumsUrl + `/${id}/.json`).pipe(
      map(album => album) // JSON
    );
  }


  getLastAlbumId(): Observable<number> {
    let count = 0;

    return this.getAlbums().pipe(
      map(albums => {
        albums.forEach(() => {
          count++;
        }
        )
        return count;
      })
    )

  }

  addAlbum(album: Album): Observable<Album> {
    return this.http.post<Album>(this.albumsUrl + '/.json', album);
  }

  deleteAlbum(id: string): Observable<void> {
    return this.http.delete<void>(this.albumsUrl + `/${id}/.json`);
  }

  paginate(start: number, end: number): Observable<Album[]> {
    
    return this.getAlbums().pipe(
      map(albums => {
        return albums.slice(start, end)
      }));
      
  }
  /*
    getCountAlbums()
    {
      return this._albums.length;
    }
  */
  getAlbumList(id: string): Observable<AlbumList> {
    // URL/ID/.json pour récupérer un album
    return this.http.get<AlbumList>(this.albumListsUrl + `/${id}/.json`).pipe(
      map(albumlist => albumlist) // JSON
    );
  }

  searchAlbums(word: string): Observable<Album[]> {
    return this.getAlbums().pipe(
      map(albums => {
        let response = [];
        albums.forEach(album => {
          if (album.title.toLowerCase().includes(word.toLowerCase())) {
            response.push(album);
          }
        });

        return response;
      })
    )
  }

  currentPage(pageNumber: number) {
    return this.sendCurrentNumberPage.next(pageNumber);
  }

  switchOn(album: Album): void {
    album.status = 'on';
    this.http.put<void>(this.albumsUrl + `/${album.id}/.json`, album).subscribe(
      e => e,
      error => console.warn(error),
      () => {
        this.subjectAlbum.next(album);
      }
    );
  }

  switchOff(album: Album) {
    album.status = 'off';
    this.http.put<void>(this.albumsUrl + `/${album.id}/.json`, album).subscribe(
      e => e,
      error => console.warn(error),
    );
  }

}
