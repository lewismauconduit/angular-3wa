import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
//import { environment } from '../../environments/environment';

import { Album } from '../album';
import { AlbumService } from '../album.service';

@Component({
  selector: 'app-paginate',
  templateUrl: './paginate.component.html',
  styleUrls: ['./paginate.component.scss']
})
export class PaginateComponent implements OnInit {

  //@Input()  listLength: number;
  @Output() onChangePage: EventEmitter<Album[]> = new EventEmitter();
  @Input() length;

  albums: Album[];
  numberOfPages: number;
  pages = [];
  currentPage: number;

  constructor(private albumService: AlbumService) 
  { 
     
  }

  ngOnInit() 
  {

    this.albumService.sendCurrentNumberPage.subscribe(numberPage => {
                                          this.currentPage = numberPage;
    }); // écoute du subject sendCurrentNumberPage

    this.albumService.currentPage(1); // initialisation du subject

    this.albumService.getAlbums().subscribe(
              albums => {
                    this.albums = albums;
                    let albumsCopy = this.albums.slice();
                    this.numberOfPages = Math.ceil(albumsCopy.length/this.length);

                    while (albumsCopy.length > this.length)
                    {
                      this.pages.push(albumsCopy.splice(0, this.length));  
                    }
                    this.pages.push(albumsCopy); //ajout des derniers éléments
                  }
    )
  }
    
  onClickPaginate(page: Album[])
  {
    this.onChangePage.emit(page); // Emet à AlbumComponent le groupe d'albums à afficher
  }

  onClickPrevious()
  {
    if (this.currentPage > 1)
    {
      this.onChangePage.emit(this.pages[this.currentPage-2]);
      this.albumService.currentPage(this.currentPage-1);
    }
    
  }

  onClickNext()
  {
    if (this.currentPage < 5)
    {
      this.onChangePage.emit(this.pages[this.currentPage]);
      this.albumService.currentPage(this.currentPage+1);
    }
    
  }

  getPageNumber(i: number)
  {
    this.albumService.currentPage(i+1);
  }

}

