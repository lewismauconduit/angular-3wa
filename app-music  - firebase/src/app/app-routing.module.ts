import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumComponent } from './admin/album/album.component'
import { GuardService } from './guard.service';


const albumsRoutes: Routes = [
  {
    path: 'admin', canActivate: [GuardService],
    component: AlbumComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(albumsRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
