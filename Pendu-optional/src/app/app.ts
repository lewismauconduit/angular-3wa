/**
 * Source 
 * @author : Vous 
 */
import { MockWords } from "../data";
import { Game, Status } from "../game";
import * as process from "process"; // Typage de process pour la gestion des flux

/**
 * Bootstrap
 */
process.stdin.setEncoding('utf8'); // Définit l'encodage des caractères dans le flux de la console.

let game = new Game(MockWords); // Initialisation du jeu

//2). Le traitement des entrées se fait ici
process.stdin.on('data', (data) => {

  if (data.toString().trim() == 'yes' || data.toString().trim() == 'no')
  {
    if (data.toString().trim() == 'yes')
    {
      let game = new Game(MockWords);
      console.log(game.message);
      process.stdout.write('> ');
      game.run(data.toString().trim()); // logique du jeu
    }
    else
    {
      process.exit();
    }
  }
  else
  {
    game.run(data.toString().trim()); // logique du jeu
  }

  if (game.status == 2)
  {
    process.stdout.write('> '); 
  }

  console.log(game.message);

  if (game.status == 0 || game.status == 1)
  {
    console.log(`Voulez-vous rejouer ? Répondez yes or no :`);
    process.stdout.write('> ');
    
  }

});

// 1.) Au début ce code s'exécute puis après tout se passe dans stdin.on('data')
console.log(game.message);
process.stdout.write('> ');