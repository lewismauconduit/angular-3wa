const { of } = require('rxjs');
const { max } = require('rxjs/operators');

// liste de points // of c'est un Observable 
const source = of(
    { x: 10.5, y: -10.6 }, // 1
    { x: 5.5, y: 8.3 },  // 2
    { x: -7.3, y: 3.3 }, // 3
    { x: 8.9, y: -2.6 } // 4
);

// mapping data  TODO filtre

const pipeSource = source.pipe(
    max((a,b) => Math.sqrt(a.x**2+ a.y**2) < Math.sqrt(b.x**2+ b.y**2) ? -1 : 1)
    )
// s'inscrire TODO et afficher les données

const subscribeOne = pipeSource.subscribe(
    num => console.log(num
    ));
