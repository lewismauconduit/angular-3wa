import { Component, OnInit } from '@angular/core';
import { interval, Observable} from 'rxjs';
import { map, takeWhile } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app-music';
  time: string;
  

  ngOnInit() 
  {
    let secondes = 0;
    let minutes = 0;
    let hours = 0;

    const count = interval(1000);
    count.pipe(
            takeWhile(value => value < 12*3600),
            map(() => {
                secondes ++;

                if (secondes >= 60) 
                {
                  minutes++;
                  secondes = 0;
                }
                if (minutes >= 60)
                {
                  hours ++;
                  minutes = 0;
                }
                return `${hours}H${minutes}m${secondes}s`;
              })
            )
          .subscribe(value => { 
              this.time = value;
          })
      
  }

}
