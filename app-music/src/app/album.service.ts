import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Album } from './album';
import { AlbumList } from './album-list';
import { ALBUMS } from './mock-albums';
import { ALBUM_LISTS } from './mock-albums';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  private _albums: Album[] = ALBUMS;
  private _albumList: AlbumList[] = ALBUM_LISTS;

  // subject pour la pagination - informer les autres components
  sendCurrentNumberPage = new Subject<number>();
  
  // subject pour la barre de lecture 
  subjectAlbum = new Subject<Album>();

  constructor() 
  { 
  }

  getAlbums() //retourne tous les albums
  {
    return this._albums.sort((x,y) => y.duration - x.duration ); // tri des albums par ordre décroissant de durée
  }

  paginate(start: number, end: number)
  {
    return this.getAlbums().slice(start, end);
  }

  getCountAlbums()
  {
    return this._albums.length;
  }

  getAlbum( id: string) //retourne un album
  {
    return this._albums.find(elem => elem.id === id)
  }
  
  getAlbumList( id : string) // retourne la liste des titres d’un album
  {
    return this._albumList.find(elem => elem.id === id)
  } 
  
  searchAlbums(word: string)
  {
    return this._albums.filter(elem => elem.title.toLowerCase().includes(word.toLowerCase())) //récupère tous les albums dont le title contient la recherche
  }

  currentPage(pageNumber: number) 
  {
    return this.sendCurrentNumberPage.next(pageNumber);
  }

  switchOn(album: Album)
  {
    album.status = 'on';
    return this.subjectAlbum.next(album);
  }

  switchOff(album: Album)
  {
    album.status = 'off';
  }

}
