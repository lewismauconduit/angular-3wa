// définition de la classe
class Product {

    private _name: string;
    private _ref: string;

    constructor(name: string) {
        this._name = name;
        this._ref = 'motorbike';
    }
    // setter
    set name(name: string) {
        this._name = name;
    }
    // getter
    get name(): string {
        return this._name;
    }

    get ref(): string {
        return this._ref;
    }
}
// instance de la classe
let bike = new Product('Super Bike');
console.log(`name: ${bike.name} - ref: ${bike.ref}`);

