interface Duck
{
    name : string ;
    swim(): string;
}

class Thing implements Duck
{
    public name: string;

    swim()
    {
        return `Je nage`;
    }
}

let thing = new Thing();
console.log(thing.swim());