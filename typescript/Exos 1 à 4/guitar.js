class Music {
    constructor() {
        this._instrument = 'nothing';
    }
    play() {
        return "play music";
    }
}
class Guitar extends Music {
    constructor() {
        super();
        this._instrument = `Guitarfolk`;
    }
    makeSound() {
        return `Je fais du son`;
    }
    get instrument() {
        return this._instrument;
    }
}
let guitar = new Guitar();
console.log(guitar.instrument);
console.log(guitar.makeSound());
//# sourceMappingURL=guitar.js.map