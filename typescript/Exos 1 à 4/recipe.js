class Recipe {
    constructor(name, star) {
        this.name = name;
        this.star = star;
    }
}
let recipes = [
    { name: 'Risotto', star: 2 },
    { name: 'Gratin' }
];
recipes.forEach(element => {
    if (element.star != undefined) {
        console.log(`${element.name} a ${element.star} étoiles`);
    }
    else {
        console.log(`${element.name}`);
    }
});
//# sourceMappingURL=recipe.js.map