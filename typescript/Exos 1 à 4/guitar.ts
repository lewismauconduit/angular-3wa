abstract class Music {

    protected _instrument: string = 'nothing';
    abstract makeSound():string;

    play(): string {
    return "play music";
    }
}

class Guitar extends Music
{
    constructor()
    {
        super();
        this._instrument = `Guitarfolk`;
    }

    makeSound()
    {
        return `Je fais du son`;
    }

    get instrument(): string
    {
        return this._instrument;
    }
}

let guitar = new Guitar();
console.log(guitar.instrument);
console.log(guitar.makeSound());