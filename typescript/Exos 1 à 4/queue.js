class Queue {
    constructor() {
        this.tab = [];
    }
    push(value) {
        this.tab.push(value);
    }
    pop() {
        this.tab.pop();
        return this.tab[0];
    }
}
var Types;
(function (Types) {
    Types[Types["Number"] = 0] = "Number";
    Types[Types["String"] = 1] = "String";
    Types[Types["Array"] = 2] = "Array";
})(Types || (Types = {}));
let queue = new Queue();
queue.push(1);
queue.push(2);
queue.push(3);
queue.push(4);
console.log(queue.pop()); // affiche 1
console.log(queue.tab);
//# sourceMappingURL=queue.js.map