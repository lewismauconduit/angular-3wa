class Queue<T> 
{
    tab = [];

    push(value :T)
    {
        this.tab.push(value);
    }

    pop()
    {
        this.tab.pop();
        return this.tab[0];
    }
}

enum Types{
    Number,
    String,
    Array
}

let queue = new Queue<Types.Number>();
queue.push(1);
queue.push(2);
queue.push(3);
queue.push(4);
console.log(queue.pop()); // affiche 1
console.log(queue.tab); 