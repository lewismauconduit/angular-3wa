class Recipe 
{
    name: string;
    star?: number; // ?NameAttribut <=> attribut facultatif

    constructor(name, star)
    {
        this.name = name;
        this.star = star;
    }
}

let recipes: Recipe[] = [
                            {name:'Risotto', star: 2},
                            {name:'Gratin'}
                        ]

recipes.forEach(element => {
    if (element.star != undefined)
    {
        console.log(`${element.name} a ${element.star} étoiles`);
    }
    else
    {
        console.log(`${element.name}`);
    }
    
});