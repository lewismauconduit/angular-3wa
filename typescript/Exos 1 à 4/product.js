// définition de la classe
class Product {
    constructor(name) {
        this._name = name;
        this._ref = 'motorbike';
    }
    // setter
    set name(name) {
        this._name = name;
    }
    // getter
    get name() {
        return this._name;
    }
    get ref() {
        return this._ref;
    }
}
// instance de la classe
let bike = new Product('Super Bike');
console.log(`name: ${bike.name} - ref: ${bike.ref}`);
//# sourceMappingURL=product.js.map