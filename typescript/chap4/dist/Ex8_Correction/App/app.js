// Exercice 8.1 et 8.2
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
function readonly(target, key, descriptor) {
    descriptor.writable = false; // modifier la visibilité à true si vous voulez redéfinir la méthode
    return descriptor;
}
// decorateur callable avec passage de paramètres
function modify(model) {
    var newModel = model;
    return function (target, key, descriptor) {
        descriptor.value = function () { return newModel; };
    };
}
var Bike = /** @class */ (function () {
    function Bike() {
    }
    Bike.prototype.speed = function () {
        return 8;
    };
    Bike.prototype.model = function () { return 'normal'; };
    Bike.prototype.price = function () {
        return 1500;
    };
    __decorate([
        readonly
    ], Bike.prototype, "speed", null);
    __decorate([
        modify('electric')
    ], Bike.prototype, "model", null);
    return Bike;
}());
var bike = new Bike;
bike.speed = function () { return 5; }; // on tente de redéfinir la méthode impossible car writable à false
// la méthode retournera 8 car elle n'est accessible qu'en lecture 
console.log(bike.speed());
console.log(bike.model()); // modification du modèle
function Feature(config) {
    return function (target) {
        // Permet de définir une nouvelle propriété 
        Object.defineProperty(target.prototype, // Nom de la classe Duck 
        'swim', // nom de la fonction que l'on ajoute
        // le descripteur on définit ici la méthode elle-même
        {
            value: function () { return config.action; },
        });
    };
}
var Duck = /** @class */ (function () {
    function Duck() {
    }
    Duck.prototype.say = function () { return "Je suis un oiseau"; };
    Duck = __decorate([
        Feature({
            action: "Nage comme un canard"
        })
    ], Duck);
    return Duck;
}());
var duck = new Duck;
console.log(duck.swim());
console.log(duck.say());
