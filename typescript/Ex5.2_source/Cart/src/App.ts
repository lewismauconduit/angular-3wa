
// Importez les bons fichiers et définition et une fois que vous avez récupérez tous les products mappez ce dernier 
// pour extraire uniquement les produits dont l'option delivery est "special"

import { Product } from "./Product";
import { Details, Delivery, MockDetails, MockDelivery } from "./data/MockProducts";


let products1 = new Product(MockDetails, MockDelivery);

let productschoosen = [];

products1.option.forEach(child => {
                            if (child.delivery == 'special')
                            {
                                productschoosen.push(child.id);
                            }
});

//console.log(productschoosen);

/*************CORRECTION************/

let products: Array<Product<Details, Delivery>> = [];

MockDetails.forEach((detail) => {
    let mockDelivery = MockDelivery.find(delivery => delivery.id == detail.id);
    products.push(new Product<Details, Delivery>(detail, mockDelivery.delivery));
});

// Affichez que les produits dont l'option est "spécial"

let specialProducts = products.filter(product => product.option === Delivery.Special );

console.log(specialProducts);
// console.log(products);