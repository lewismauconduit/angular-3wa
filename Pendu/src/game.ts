import { Word } from "./data";

// Etat du jeu
export enum Status {
    Winner,
    Loser,
    Progress
}

export class Game {

    private _attempts: number; // nombre de coups
    private readonly _maxAttempts: number = 3; // définir le nombre de coups max en lecture seule
    private _word: string; // mot à devnier en clair
    //private _hiddenWord: string; // caché le mot
    private _status: Status; // état du jeu Progress, Winner ou Loser
    private _message: string; // message à destination du joueur

    constructor(private _words: Array<Word>) {
        this.init(_words); 
    }

    /**
     * init : intialiser le jeu 
     * 
     * @param words 
     */
    init(words: Array<Word>): void {
        this._attempts = 0;
        this._status = Status.Progress;

        let alea = Math.floor(Math.random() * Math.floor(words.length));
        this._word = words[alea].word;
        this._message = `Bienvenue au jeu du pendu. Vous devez deviner le mot caché en ${this._maxAttempts} coups. Voici le mot caché : ${this.hide(this._word)}`;

    }

    // getter et setter
    get status(): Status { return this._status; }
    set status(status: Status) { this._status = status; }
    get message(): string { return this._message; }
    // gestion de l'utilisateur : setter et getter
    get attemtps(): number { return this._attempts; }

    /**
    * isWord : test boolean si le mot est celui que l'on cherche ou non
    * 
    * @param word 
    */
    isWord(word: string) {

        if (word == this._word)
        {
            return true;

        }
        else
        {
            return false;
            
        }
    }

    /**
     * show : affiche le mot caché à deviner
     */
    show(): string {
        return this._word; 
    }

    /**
     * run: logique du jeu
     * 
     * @param choice 
     */
    run(choice: string): void {
        if (this._attempts < this._maxAttempts-1 && this._status != Status.Winner)
        {
            //this.isWord(choice);
            if (this.isWord(choice))
            {
                this._attempts++;
                this._status = Status.Winner;
                this.final();
            }
            else
            {
                this._attempts++;
                this._message = `Dommage mais ${choice} n'est pas le mot caché.
                        Recommencez, ${this.hide(this._word)}
                        Nombre de coups restants: ${this._maxAttempts - this._attempts}`;
            }
        }
        else if (this._attempts >= this._maxAttempts-1)
        {
            this._status = Status.Loser;
            this.final();
        }
 
    }

    /**
     * final: affiche l'état du jeu à la fin
     */
    final() 
    {
        if (this._status == Status.Winner)
        {
            this._message = `Vous avez trouvé le mot ${this.show()} en ${this._attempts} tentative(s).
                            Vous avez gagné. 
                            Félicitations.`;
        }
        else 
        {
            this._message = `Le mot caché était ${this._word}.
                            Vous avez perdu. Réessayez une autre fois.`;
        }
      
    }

    /**
     * hide: affiche l'état du jeu à la fin
     */
    hide(wordClear)
    {
        let length = wordClear.length;

        //Couper le mot en trois parties
        let part1 = wordClear.slice(0, length/3);
        let part2 = wordClear.slice(length/3, length*2/3);
        let part3 = wordClear.slice(length*2/3, length);
        
        let part1Hidden = part1.replace(/[A-Za-z]/g, '#'); // remplacer l'intégralité de la partie 1
        let part2Hidden = part2.substr(0, 1)+part2.substr(1, part2.length).replace(/[A-Za-z]/g, '#'); // remplacer la première lettre de la partie 2
        let part3Hidden = part3.substr(0, part3.length-1).replace(/[A-Za-z]/g, '#')+part3.substr(part3.length-1, part3.length);// remplacer la dernière lettre de la partie 3

        return part1Hidden+part2Hidden+part3Hidden;

    }

}