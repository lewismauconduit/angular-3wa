"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Etat du jeu
var Status;
(function (Status) {
    Status[Status["Winner"] = 0] = "Winner";
    Status[Status["Loser"] = 1] = "Loser";
    Status[Status["Progress"] = 2] = "Progress";
})(Status = exports.Status || (exports.Status = {}));
class Game {
    constructor(_words) {
        this._words = _words;
        this._maxAttempts = 3; // définir le nombre de coups max en lecture seule
        this.init(_words);
    }
    /**
     * init : intialiser le jeu
     *
     * @param words
     */
    init(words) {
        this._attempts = 0;
        this._status = Status.Progress;
        let alea = Math.floor(Math.random() * Math.floor(words.length));
        this._word = words[alea].word;
        this._message = `Bienvenue au jeu du pendu. Vous devez deviner le mot caché en ${this._maxAttempts} coups. Voici le mot caché : ${this.hide(this._word)}`;
    }
    // getter et setter
    get status() { return this._status; }
    set status(status) { this._status = status; }
    get message() { return this._message; }
    // gestion de l'utilisateur : setter et getter
    get attemtps() { return this._attempts; }
    /**
    * isWord : test boolean si le mot est celui que l'on cherche ou non
    *
    * @param word
    */
    isWord(word) {
        if (word == this._word) {
            return true;
        }
        else {
            return false;
        }
    }
    /**
     * show : affiche le mot caché à deviner
     */
    show() {
        return this._word;
    }
    /**
     * run: logique du jeu
     *
     * @param choice
     */
    run(choice) {
        if (this._attempts < this._maxAttempts - 1 && this._status != Status.Winner) {
            //this.isWord(choice);
            if (this.isWord(choice)) {
                this._attempts++;
                this._status = Status.Winner;
                this.final();
            }
            else {
                this._attempts++;
                this._message = `Dommage mais ${choice} n'est pas le mot caché.
                        Recommencez, ${this.hide(this._word)}
                        Nombre de coups restants: ${this._maxAttempts - this._attempts}`;
            }
        }
        else if (this._attempts >= this._maxAttempts - 1) {
            this._status = Status.Loser;
            this.final();
        }
    }
    /**
     * final: affiche l'état du jeu à la fin
     */
    final() {
        if (this._status == Status.Winner) {
            this._message = `Vous avez trouvé le mot ${this.show()} en ${this._attempts} tentative(s).
                            Vous avez gagné. 
                            Félicitations.`;
        }
        else {
            this._message = `Le mot caché était ${this._word}.
                            Vous avez perdu. Réessayez une autre fois.`;
        }
    }
    /**
     * hide: affiche l'état du jeu à la fin
     */
    hide(wordClear) {
        let length = wordClear.length;
        let part1 = wordClear.slice(0, length / 3);
        let part2 = wordClear.slice(length / 3, length * 2 / 3);
        let part3 = wordClear.slice(length * 2 / 3, length);
        let part1Hidden = part1.replace(/[A-Za-z]/g, '#');
        let part2Hidden = part2.substr(0, 1) + part2.substr(1, part2.length).replace(/[A-Za-z]/g, '#');
        let part3Hidden = part3.substr(0, part3.length - 1).replace(/[A-Za-z]/g, '#') + part3.substr(part3.length - 1, part3.length);
        return part1Hidden + part2Hidden + part3Hidden;
    }
}
exports.Game = Game;
